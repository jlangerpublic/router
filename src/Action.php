<?php declare(strict_types=1);

namespace JLanger\Router;

use Exception;
use function get_class;

/**
 * Class Action
 *
 * Default Router-Service-Class.
 */
class Action
{
    /**
     * Will be called if Service is called with GET-Method.
     * @throws Exception
     */
    public function onGet(): void
    {
        throw new Exception('Method GET is not supported in Service ' . get_class($this));
    }

    /**
     * Will be called if Service is called with POST-Method.
     * @throws Exception
     */
    public function onPost(): void
    {
        throw new Exception('Method POST is not supported in Service ' . get_class($this));
    }

    /**
     * Will be called if Service is called with any Method except POST or GET.
     * @throws Exception
     */
    public function onRequest(): void
    {
        throw new Exception('onRequest-body is empty. You must specify a Method.');
    }
}
