<?php
declare(strict_types=1);

namespace JLanger\Router;

final class CSVRouteStorage implements RouteStorageInterface
{

    private string $fileName;
    
    private string $seperator;

    public function __construct(string $fileName, string $seperator = ';')
    {
        $this->fileName  = $fileName;
        $this->seperator = $seperator;
    }

    /** {@inheritDoc} */
    public function getRoutes(): array
    {
        $routes = [];
        $handle = fopen($this->fileName, "r");
        if ($handle === false) {
            return [];
        }
        while (($data = fgetcsv($handle, 1000, ",")) !== false) {
            if ($data !== null) {
                $this->addRoute($data, $routes);
            }
        }
        fclose($handle);

        return $routes;
    }

    /**
     * @param array<string> $data
     * @param array<Route> $routes
     */
    private function addRoute(array $data, array &$routes): void
    {
        [$url, $method, $phpFileToExecute] = $data;
        $route                             = $this->findRoute($url, $routes);
        if ($route === null) {
            $routes[] = new Route($url, [$method => $phpFileToExecute]);
        } else {
            $route->addMethodMapping($method, $phpFileToExecute);
        }
    }

    /**
     * @param string $url
     * @param array<Route> $routes
     * @return Route|null
     */
    private function findRoute(string $url, array $routes): ?Route
    {
        foreach ($routes as $route) {
            if ($route->getPath() === $url) {
                return $route;
            }
        }

        return null;
    }
}
