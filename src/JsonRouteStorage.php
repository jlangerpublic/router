<?php
declare(strict_types=1);

namespace JLanger\Router;

final class JsonRouteStorage implements RouteStorageInterface
{
    
    private string $fileName;

    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;
    }

    /** {@inheritDoc} */
    public function getRoutes(): array
    {
        $fileContents = file_get_contents($this->fileName);
        if ($fileContents !== false) {
            $json   = json_decode($fileContents, true, 512, JSON_THROW_ON_ERROR);
            $routes = [];
            foreach ($json as $routeName => $routeConfiguration) {
                $routes[] = new Route($routeName, $routeConfiguration);
            }

            return $routes;
        }
        return [];
    }
}
