<?php
declare(strict_types=1);

namespace JLanger\Router;

use InvalidArgumentException;

class Route
{
    private string $path;

    /** @var array<string, string> */
    private array $methodMapping;

    /**
     * Route constructor.
     * @param string $route
     * @param array<string, string> $methodMapping Key: Name of the method, Value: File or service to execute.
     */
    public function __construct(string $route, array $methodMapping)
    {
        $this->path          = $route;
        $this->methodMapping = $methodMapping;
        if (count($this->methodMapping) <= 0) {
            throw new InvalidArgumentException('No mapping for route path: ' . $this->path);
        }
    }

    public function addMethodMapping(string $method, string $phpFileToExecute): void
    {
        $this->methodMapping[$method] = $phpFileToExecute;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getConfiguration(string $requestMethod = null): string
    {
        foreach ($this->methodMapping as $method => $targetFile) {
            if ($method === $requestMethod || $method === '*') {
                return $targetFile;
            }
        }
        throw new InvalidArgumentException("Route ({$this->path}) has no configuration for method ($requestMethod)");
    }

    public function hasConfiguration(string $requestMethod): bool
    {
        if (array_key_exists($requestMethod, $this->methodMapping)) {
            return true;
        }
        return array_keys($this->methodMapping)[0] === '*';
    }
}
