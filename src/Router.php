<?php
declare(strict_types=1);

namespace JLanger\Router;

use Exception;
use InvalidArgumentException;
use JLanger\Di\Container;
use JLanger\Router\RouteHandler\IncludingRouteHandler;
use JLanger\Router\RouteHandler\RouteHandlerInterface;
use JLanger\Router\RouteHandler\ServiceBasedRouteHandler;
use function explode;
use function strlen;

class Router
{
    private const REQUEST_METHOD = 'REQUEST_METHOD';
    private const REQUEST_URI    = 'REQUEST_URI';

    public string $rewriteBase;

    private RouteStorageInterface $routeStorage;

    private RouteHandlerInterface $routeHandler;

    /**
     * Router constructor.
     *
     * @param RouteStorageInterface      $routeStorage
     * @param string                     $rewriteBase
     * @param RouteHandlerInterface|null $routeHandler
     *
     * @throws Exception
     */
    public function __construct(
        RouteStorageInterface $routeStorage,
        string $rewriteBase = '',
        ?RouteHandlerInterface $routeHandler = null
    ) {
        $this->routeStorage = $routeStorage;
        $this->rewriteBase  = $rewriteBase;
            
        $this->routeHandler = $routeHandler ?? new IncludingRouteHandler();
    }

    /**
     * @param array<string, string> $phpRouteInfo
     *
     * @return Route|null
     * @throws InvalidArgumentException
     */
    public function match(array $phpRouteInfo): ?Route
    {
        [self::REQUEST_METHOD => $requestMethod, self::REQUEST_URI => $requestPath] = $phpRouteInfo;

        $requestPath = explode('?', $requestPath, 2)[0];
        if ($requestPath !== false) {
            if (strlen($this->rewriteBase) > 0) {
                $requestPath = explode($this->rewriteBase, $requestPath, 2);
                if ($requestPath !== false) {
                    $requestPath = $requestPath[1];
                } else {
                    $requestPath = '';
                }
            }
        }

        // @var Route[] $routes

        $routes        = $this->routeStorage->getRoutes();
        $matchedRoutes = [];
        foreach ($routes as $route) {
            if ($route->getPath() !== $requestPath) {
                continue;
            }
            $matchedRoutes[] = $route;
        }

        if (count($matchedRoutes) === 0) {
            return null;
        }

        if (count($matchedRoutes) > 1) {
            throw new InvalidArgumentException('More than one route matches the given configuration');
        }

        return $matchedRoutes[0];
    }

    /**
     * @param array<string, string> $phpRouteInfo
     *
     * @throws Exception
     */
    public function doRouting(array $phpRouteInfo): void
    {
        $matchedRoute = $this->match($phpRouteInfo);
        $method       = $phpRouteInfo[self::REQUEST_METHOD];

        $this->routeHandler->handleRoute($matchedRoute, $method);
    }
}
