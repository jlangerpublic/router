<?php

declare(strict_types=1);

namespace JLanger\Router\RouteHandler;

use JLanger\Router\Route;

/**
 * Handles a route by reading it's configuration and including the file specified.
 * This class is here for backwards compatibility with pervious versions.
 *
 * @deprecated use JLanger\Router\RouteHandler\ServiceBasedRouteHandler instead
 */
class IncludingRouteHandler implements RouteHandlerInterface
{
    public function handleRoute(?Route $route, string $requestMethod): void
    {
        if ($route === null) {
            http_response_code(404);
            return;
        }

        if (!$route->hasConfiguration($requestMethod)) {
            http_response_code(405);
        }

        require_once $route->getConfiguration($requestMethod);
    }
}
