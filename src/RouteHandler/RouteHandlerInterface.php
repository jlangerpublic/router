<?php

declare(strict_types=1);

namespace JLanger\Router\RouteHandler;

use JLanger\Router\Route;

interface RouteHandlerInterface
{

    public function handleRoute(?Route $route, string $requestMethod): void;
}
