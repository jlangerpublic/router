<?php

declare(strict_types=1);

namespace JLanger\Router\RouteHandler;

use Exception;
use JLanger\Di\Container;
use JLanger\Router\Action;
use JLanger\Router\Route;

/**
 * Handles routing by loading a service from the service container and executing a function of the service.
 */
class ServiceBasedRouteHandler implements RouteHandlerInterface
{
    private Container $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param Route|null $route
     * @param string     $requestMethod
     *
     * @throws Exception
     */
    public function handleRoute(?Route $route, string $requestMethod): void
    {
        if ($route === null) {
            http_response_code(404);
            return;
        }

        if (!$route->hasConfiguration($requestMethod)) {
            http_response_code(405);
        }

        // Loading a service and executing a predefined method on it.
        
        $service = $this->container->get($route->getConfiguration($requestMethod));
        if (!($service instanceof Action)) {
            throw new Exception('The requested Service does not extend Action-Class.');
        }
        
        switch ($requestMethod) {
            case 'GET':
                $service->onGet();
                break;
            case 'POST':
                $service->onPost();
                break;
            default:
                $service->onRequest();
                break;
        }
    }
}
