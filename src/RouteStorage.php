<?php
declare(strict_types=1);

namespace JLanger\Router;

use InvalidArgumentException;

class RouteStorage implements RouteStorageInterface
{
    /** @var RouteStorageInterface[] */
    private array $routeStorages;

    /**
     * RouteStorage constructor.
     *
     * @param RouteStorageInterface[] $routeStorages
     * @throws InvalidArgumentException
     */
    public function __construct(array $routeStorages)
    {
        foreach ($routeStorages as $routeStorage) {
            if (!$routeStorage instanceof RouteStorageInterface) {
                throw new InvalidArgumentException('$routeStorages must only contain instances of RouteStorageInterface');
            }
        }
        $this->routeStorages = $routeStorages;
    }

    /**
     * @inheritDoc
     */
    public function getRoutes(): array
    {
        $routes = [];
        foreach ($this->routeStorages as $routeStorage) {
            $routes = \array_merge($routes, $routeStorage->getRoutes());
        }
        
        return $routes;
    }
}
