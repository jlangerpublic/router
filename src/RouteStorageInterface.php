<?php
declare(strict_types=1);

namespace JLanger\Router;

interface RouteStorageInterface
{
    /** @return Route[] */
    public function getRoutes(): array;
}
