<?php
declare(strict_types=1);

use JLanger\Di\ContainerBuilder;
use JLanger\Router\CSVRouteStorage;
use JLanger\Router\JsonRouteStorage;
use JLanger\Router\RouteHandler\ServiceBasedRouteHandler;
use JLanger\Router\Router;
use JLanger\Router\RouteStorage;

require_once __DIR__ . '/../vendor/autoload.php';
$rs = new RouteStorage([new JsonRouteStorage(__DIR__.'/routes.json')]);

class Action
{
    public function onRequest()
    {
        echo "Hello I am handling a request";
    }
}

// Building the container
$builder = new ContainerBuilder();
$builder->addFile(__DIR__.'/services.xml');
$container = $builder->getContainer();

$_SERVER = [
    'REQUEST_URI' => '/',
    'REQUEST_METHOD' => 'POST'
];

$routeHandler = new ServiceBasedRouteHandler($container);
(new Router($rs, '', $routeHandler))->doRouting($_SERVER);
