# Simple Router
This project implements a very simple Router based in php.

The configuration for it looks like this:
```json
{
  "/homepage": {
    "GET": "homepage.php"
  },
  "/other_page": {
    "*": "other_page.php"
  }
}
```
This will allow only get methods on `/homepage` and all methods on `/other-page`.

All paths in the configuration must be relative to the index document mentioned in the usage section.

## Usage
```php
<?php

include 'vendor/autoload.php';

use JLanger\Router\Router;
use JLanger\Router\JsonRouteStorage;

(new Router(new JsonRouteStorage('config_file.json')))->doRouting($_SERVER);
```
